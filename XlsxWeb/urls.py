from XlsxWeb import settings
from XlsxWeb.views import IndexView
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin

urlpatterns = static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

urlpatterns += [
    url(r'', include('SheetManager.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^.*$', IndexView.as_view())
]
