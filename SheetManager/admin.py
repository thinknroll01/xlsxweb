from SheetManager.models import SpreadSheet
from django.contrib import admin

# Register your models here.
class SpreadSheetAdmin(admin.ModelAdmin):
    fields = ('name', 'spreadsheet_file')
    list_display = ('id', 'name', 'spreadsheet_file', 'csv_file')


admin.site.register(SpreadSheet, SpreadSheetAdmin)
