# -*- coding: UTF-8 -*-
import os
import hashlib
from XlsxWeb import settings
from xlsx2csv import Xlsx2csv
from django.core.files import File
from django.db import models


def get_spreadsheet_path(instance, filename):
    try:
        extension = filename.rsplit('.', 1)[1]
        md5_name = '{0}.{1}'.format(hashlib.md5(filename).hexdigest(), extension)
    except IndexError:
        md5_name = hashlib.md5(filename).hexdigest()
    return '{0}/{1}'.format("spreadsheet", md5_name)


def get_csv_path(instance, filename):
    try:
        extension = filename.rsplit('.', 1)[1]
        md5_name = '{0}.{1}'.format(hashlib.md5(filename).hexdigest(), extension)
    except IndexError:
        md5_name = hashlib.md5(filename).hexdigest()
    return '{0}/{1}'.format("csv", md5_name)


class SpreadSheet(models.Model):
    spreadsheet_file = models.FileField(upload_to=get_spreadsheet_path)
    csv_file = models.FileField(upload_to=get_csv_path, null=True)
    name = models.CharField(unique=True, max_length=200)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def save(self, *args, **kwargs):
        super(SpreadSheet, self).save(*args, **kwargs)
        if not self.csv_file:
            tmp_file = "/tmp/{0}".format(self.name)
            Xlsx2csv(self.spreadsheet_file.path).convert(tmp_file)
            f = File(open(tmp_file))
            self.csv_file.save(self.name, f)
            return

    def __unicode__(self):
        return self.name.encode('utf8')

