from SheetManager.models import SpreadSheet
from SheetManager.serializers import SpreadSheetSerializer
from django.shortcuts import get_object_or_404
from rest_framework import status
from rest_framework.parsers import FileUploadParser
from rest_framework.response import Response
from rest_framework.views import APIView


class APISheetView(APIView):

    def get(self, request, pk=None):
        if pk is None:
            sheets = SpreadSheet.objects.all()
            serializer = SpreadSheetSerializer(sheets, many=True)
            return Response(serializer.data)
        sheet = get_object_or_404(SpreadSheet, pk=pk)
        serializer = SpreadSheetSerializer(sheet)
        return Response(serializer.data)

    def post(self, request, pk=None):
        if pk is None:
            serializer = SpreadSheetSerializer(data=request.data)
            serializer.is_valid(True)
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        sheet = get_object_or_404(SpreadSheet, pk=pk)
        serializer = SpreadSheetSerializer(sheet, data=request.data, partial=True)
        return Response(serializer.data)


class APISheetUploadView(APIView):
    parser_classes = (FileUploadParser,)

    def post(self, request):
        file_obj = request.FILES['file']
        name = file_obj.name
        try:
            if name.rsplit('.', 1)[1] != 'xlsx':
                return Response("File type not supported.", status=status.HTTP_400_BAD_REQUEST)
        except IndexError:
            return Response("File's not contain extension.", status=status.HTTP_400_BAD_REQUEST)
        try:
            sheet = SpreadSheet.objects.get(name=name)
        except SpreadSheet.DoesNotExist:
            sheet = SpreadSheet.objects.create(name=name, spreadsheet_file=file_obj)
            return Response("File '%s' created" % sheet.name, status=status.HTTP_201_CREATED)
        sheet.spreadsheet_file = file_obj
        sheet.save()
        return Response("File '%s' Updated" % sheet.name, status=status.HTTP_200_OK)


class APISheetCSVView(APIView):

    def get(self, request, pk):
        sheet = get_object_or_404(SpreadSheet, pk=pk)
        # csv = []
        # with open(sheet.csv_file.path, "r") as f:
        lines = [line.rstrip('\n').split(",") for line in open(sheet.csv_file.path, "r")]
        return Response(lines)
