from SheetManager.models import SpreadSheet
from rest_framework import serializers


class SpreadSheetSerializer(serializers.ModelSerializer):

    class Meta:
        model = SpreadSheet
        fields = ('id', 'name', 'spreadsheet_file', 'csv_file', 'updated_at')
        read_only_fields = ('id',)

