# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import SheetManager.models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='SpreadSheet',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('spreadsheet_file', models.FileField(upload_to=SheetManager.models.get_spreadsheet_path)),
                ('csv_file', models.FileField(null=True, upload_to=SheetManager.models.get_csv_path)),
                ('name', models.CharField(unique=True, max_length=200)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
        ),
    ]
