from django.conf.urls import url
import views

urlpatterns = [
    url(r'^api/sheet/upload/$', views.APISheetUploadView.as_view(), name='api_sheet_upload'),
    url(r'^api/sheet/csv/(?P<pk>\d+)/$', views.APISheetCSVView.as_view(), name='api_sheet_csv'),
    url(r'^api/sheet/((?P<pk>\d+)/)?$', views.APISheetView.as_view(), name='api_sheet_view'),
]