/**
 * application modules declaration
 * @namespace app
 */
(function () {
    'use strict';

    angular
        .module('app', [
            'app.config',
            'app.routes',
            'app.spreadsheet'
            //'app.layout',
            //'app.authentication',
        ]);

    angular
        .module('app.config', []);

    angular
        .module('app.routes', ['ngRoute']);

    angular
        .module('app')
        .run(run);

    run.$inject = ['$http'];

    /**
     * @name run
     * @desc Action performed when application instantiated
     */
    function run($http) {
        $http.defaults.xsrfHeaderName = 'X-CSRFToken';
        $http.defaults.xsrfCookieName = 'csrftoken';
    }
})();