/**
 * ngRoute configure
 * @namespace app.routes
 */
(function () {
    'use strict';

    angular
        .module('app.routes')
        .config(config);

    config.$inject = ['$routeProvider'];

    /**
     * @name config
     * @desc Define valid application routes
     */
    function config($routeProvider) {
        $routeProvider
            .when('/', {
                controller: 'ListPageController',
                controllerAs: 'vm',
                templateUrl: '/static/templates/spreadsheet/list_page.html'
            })
            .when('/sheet/:id', {
                controller: 'SpreadsheetController',
                controllerAs: 'vm',
                templateUrl: '/static/templates/spreadsheet/sheet.html'
            })
            .otherwise({redirectTo: '/'});
    }

})();