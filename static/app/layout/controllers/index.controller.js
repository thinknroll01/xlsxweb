/**
* IndexController
* @namespace app.layout.controllers
*/
(function () {
  'use strict';

  angular
    .module('app.layout.controllers')
    .controller('IndexController', IndexController);

  IndexController.$inject = ['$scope', 'Authentication'];

  /**
  * @namespace IndexController
  */
  function IndexController($scope, Authentication) {
    var vm = this;

    vm.isAuthenticated = Authentication.isAuthenticated();
  }
})();