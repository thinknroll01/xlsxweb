/**
 * Navbar controller
 * @namespace app.layout.controllers
 */
(function () {
    'use strict';

    angular
        .module('app.layout.controllers')
        .controller('NavbarController', NavbarController);

    NavbarController.$inject = ['Authentication'];

    function NavbarController(Authentication) {
        var vm = this;

        vm.isAuthenticated = isAuthenticated;
        vm.logout = logout;

        /**
         * @name isAuthenticated
         * @desc Check that the user has been authenticated
         * @returns {boolean}
         * @memberOf app.layout.controllers.NavbarController
         */
        function isAuthenticated() {
            return Authentication.isAuthenticated();
        }

        /**
         * @name logout
         * @desc Log the user out
         * @memberOf app.layout.controllers.NavbarController
         */
        function logout() {
            Authentication.logout();
        }
    }
})();