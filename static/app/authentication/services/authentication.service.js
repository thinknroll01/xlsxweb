/**
 * Authentication
 * @namespace app.authentication.services
 */
(function () {
    'use strict';

    angular.module('app.authentication.services')
        .factory('Authentication', Authentication);

    Authentication.$inject = ['$cookies', '$http', '$location'];

    /**
     * @namespace Authentication
     * @returns {Factory}
     */
    function Authentication($cookies, $http, $location) {
        /**
         * @name Authentication
         * @desc The Factory to be returned
         */
        var Authentication = {
            login: login,
            logout: logout,
            register: register,
            getAuthenticatedAccount: getAuthenticatedAccount,
            isAuthenticated: isAuthenticated,
            setAuthenticatedAccount: setAuthenticatedAccount,
            unauthenticate: unauthenticate
        };

        active();

        return Authentication;

        ///////////////////////

        function active () {
            var account = Authentication.getAuthenticatedAccount();
            $http
                .post('/api/v1/auth/check/',{})
                .success(onCheckSuccess)
                .error(onCheckError);

            function onCheckSuccess (data, status, headers, config) {
                if (account == null) {
                    Authentication.logout();
                }
            }

            function onCheckError (data, status, headers, config) {
                Authentication.unauthenticate();
            }
        }

        /**
         * @name login
         * @desc Try to log in with given email and password
         * @param {string} email The email entered by the user
         * @param {string} password The password entered by the user
         * @returns {HttpPromise}
         * @memberOf app.authentication.services.Authentication
         */
        function login(email, password) {
            return $http
                .post('/api/v1/auth/login/', {
                    email: email, password:password
                })
                .success(onLoginSuccess)
                .error(onLoginError);

            /**
             * @name onLoginSuccess
             * @desc Save the authenticated account data and redirect to '/'
             */
            function onLoginSuccess(data, status, headers, config) {
                Authentication.setAuthenticatedAccount(data);
                $location.path('/');
            }

            /**
             * @name onLoginError
             * @desc Log 'Failure' to console
             */
            function onLoginError(data, status, headers, config) {
                console.error('Epic failure!');
            }
        }

        /**
         * @name logout
         * @desc Log the user out
         * @returns {HttpPromise}
         * @memberOf app.authentication.services.Authentication
         */
        function logout() {
            return $http
                .post('api/v1/auth/logout/',{})
                .success(onLogoutSuccess)
                .error(onLogoutError);

            /**
             * @name onLogoutSuccess
             * @desc Remove the authenticated account data and redirect to '/'
             */
            function onLogoutSuccess(data, status, headers, config) {
                Authentication.unauthenticate();
                $location.path('/');
            }

            /**
             * @name onLogoutError
             * @desc Log an error received from api
             */
            function onLogoutError(data, status, headers, config) {
                console.log(data.detail);
            }
        }

        /**
         * @name register
         * @desc Try to register a new user
         * @param {string} email The email entered by user
         * @param {string} password The password entered by user
         * @param {string} confirmPassword The confirm password entered by user
         * @param {string} username The username entered by user
         * @returns {HttpPromise}
         * @memberOf app.authentication.services.Authentication
         */
        function register(email, password, confirmPassword, username) {
            return $http
                .post('/api/v1/accounts/', {
                    username: username,
                    password: password,
                    confirm_password: confirmPassword,
                    email: email
                })
                .success(onRegisterSuccess)
                .error(onRegisterError);

            /**
             * @name onRegisterSuccess
             * @desc Log the new user in
             */
            function onRegisterSuccess(data, status, headers, config) {
                Authentication.login(email, password);
            }

            /**
             * @name onRegisterError
             * @desc Log 'Failure' to the console
             */
            function onRegisterError(data, status, headers, config) {
                console.error('Epic failure!');
            }
        }

        /**
         * @name getAuthenticatedAccount
         * @desc Return the currently authenticated account
         * @returns {object|undefined} Account if authenticated, else 'undefined'
         * @memberOf app.authentication.services.Authentication
         */
        function getAuthenticatedAccount() {
            if(!$cookies.authenticatedAccount) {
                return null;
            }
            return JSON.parse($cookies.authenticatedAccount);
        }

        /**
         * @name isAuthenticated
         * @desc Check if the current user is authenticated
         * @returns {boolean} True if the user is authenticated, else false
         * @memberOf app.authentication.services.Authentication
         */
        function isAuthenticated() {
            return !!$cookies.authenticatedAccount;
        }

        /**
         * @name setAuthenticatedAccount
         * @desc Stringify the account object and store it in a cookie
         * @param {Object} account The account object to be stored
         * @memberOf app.authentication.services.Authentication
         */
        function setAuthenticatedAccount(account) {
            $cookies.authenticatedAccount = JSON.stringify(account);
        }

        /**
         * @name unauthenticate
         * @desc Delete the cookie where the user object is stored
         * @memberOf app.authentication.services.Authentication
         */
        function unauthenticate() {
            delete $cookies.authenticatedAccount;
        }
    }

})();