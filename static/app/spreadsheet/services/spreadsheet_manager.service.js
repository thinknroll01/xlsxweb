
(function () {
    'use strict';

    angular.module('app.spreadsheet.services')
        .factory('SpreadsheetManager', SpreadsheetManager);

    SpreadsheetManager.$inject = ['$http', 'FileUploader'];

    /**
     *
     * @param $http
     * @param FileUploader
     * @returns SpreadsheetManager
     * @constructor
     */
    function SpreadsheetManager($http, FileUploader) {

        var SpreadsheetManager = {
            getUploader: getUploader,
            getSpreadsheetList: getSpreadsheetList,
            getSpreadsheet: getSpreadsheet,
            getSpreadsheetTable: getSpreadsheetTable
        };

        /////////////////////////////////
        // initial uploader

        var uploader = new FileUploader({ url: '/api/sheet/upload/' });

        uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/, filter, options) {
            console.info('onWhenAddingFileFailed', item, filter, options);
        };
        uploader.onAfterAddingFile = function(fileItem) {
            console.info('onAfterAddingFile', fileItem);
        };
        uploader.onAfterAddingAll = function(addedFileItems) {
            console.info('onAfterAddingAll', addedFileItems);
        };
        uploader.onBeforeUploadItem = function(item) {
            console.info('onBeforeUploadItem', item);
        };
        uploader.onProgressItem = function(fileItem, progress) {
            console.info('onProgressItem', fileItem, progress);
        };
        uploader.onProgressAll = function(progress) {
            console.info('onProgressAll', progress);
        };
        uploader.onSuccessItem = function(fileItem, response, status, headers) {
            console.info('onSuccessItem', fileItem, response, status, headers);
        };
        uploader.onErrorItem = function(fileItem, response, status, headers) {
            console.info('onErrorItem', fileItem, response, status, headers);
        };
        uploader.onCancelItem = function(fileItem, response, status, headers) {
            console.info('onCancelItem', fileItem, response, status, headers);
        };
        uploader.onCompleteItem = function(fileItem, response, status, headers) {
            console.info('onCompleteItem', fileItem, response, status, headers);
        };
        uploader.onCompleteAll = function() {
            console.info('onCompleteAll');
        };

        console.info('uploader', uploader);

        //////////////////////////////////

        activate();

        function activate() {

        }

        function getUploader() {
            return uploader;
        }

        function getSpreadsheetList () {
            return $http.get("/api/sheet/");
        }

        function getSpreadsheet (pk) {
            return $http.get("/api/sheet/"+pk+"/");
        }

        function getSpreadsheetTable (pk) {
            return $http.get("/api/sheet/csv/"+pk+"/");
        }

        return SpreadsheetManager;
    }
}());