
(function(){
    'use strict';

    angular
        .module('app.spreadsheet', [
            'app.spreadsheet.controllers',
            'app.spreadsheet.services'
        ]);

    angular
        .module('app.spreadsheet.controllers', ['ui.bootstrap']);

    angular
        .module('app.spreadsheet.services', ['angularFileUpload']);
})();