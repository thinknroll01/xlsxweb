(function () {
    'use strict';

    angular
        .module('app.spreadsheet.controllers')
        .controller('ListPageController', ListPageController);

    ListPageController.$inject = ['$location', 'SpreadsheetManager'];

    /**
     * @namespace ListPageController
     */
    function ListPageController($location, SpreadsheetManager) {
        var vm = this;

        vm.spreadsheetList = {};
        vm.uploader = SpreadsheetManager.getUploader();
        vm.alerts = [];
        vm.closeAlert = closeAlert;
        vm.openSheet = openSheet;

        activate();

        /**
         * @name active
         * @desc Action to be performed when this controller is instantiated
         */
        function activate() {
            refreshData();
            vm.uploader.onCompleteItem = function (fileItem, response, status, headers) {
                //vm.message = response;
                if (status == 201 || status == 200) {
                    vm.alerts.push({type: 'success', msg: response});
                } else {
                    vm.alerts.push({type: 'danger', msg: response});
                }
                refreshData();
            };
            vm.uploader.onAfterAddingFile = function(fileItem) {
                console.info('onAfterAddingFile', fileItem);
                fileItem.upload();
            };
        }

        function refreshData() {
            SpreadsheetManager.getSpreadsheetList().success(onGetListSuccess);
            function onGetListSuccess(data) {
                vm.spreadsheetList = data;
            }
        }

        function openSheet(sheet_id) {
            $location.url("/sheet/" + sheet_id + "/");
        }

        function closeAlert(index) {
            vm.alerts.splice(index, 1)
        }

    }
})();