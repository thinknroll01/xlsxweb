(function () {
    'use strict';

    angular
        .module('app.spreadsheet.controllers')
        .controller('SpreadsheetController', SpreadsheetController);

    SpreadsheetController.$inject = ['$routeParams', 'SpreadsheetManager'];

    /**
     * @namespace SpreadsheetController
     */
    function SpreadsheetController($routeParams, SpreadsheetManager) {
        var vm = this;
        vm.id = $routeParams['id'];
        vm.table = [[]];
        console.log(vm.id);

        activate();

        function activate(){
            loadTable(vm.id);
        }

        function loadTable(id) {
            SpreadsheetManager.getSpreadsheetTable(id)
                .success(onSuccess);

            function onSuccess(data) {
                console.log(data);
                vm.table = data;
            }
        }
    }
})();